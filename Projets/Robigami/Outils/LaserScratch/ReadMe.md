

# Laser Scratch

## Steps
1. Découper au laser les pièces dans du multiplex ou du MDF. 
2. Peindre les différentes pièces en fonction de leur comportement ( Structure de conditions, instructions, boucles,... )
3. Graver le nom des pièces au laser si nécessaire
4. Coller un rectangle de  2cm x 20cm de film "tableau blanc" 


![alt text](https://gitlab.com/trakk/fablab/raw/0e1962b26695823e2681b207da24cbcc760beef9/Projets/Robigami/Outils/LaserScratch/Photos/1.jpg "Photo 1")


![alt text](https://gitlab.com/trakk/fablab/raw/0e1962b26695823e2681b207da24cbcc760beef9/Projets/Robigami/Outils/LaserScratch/Photos/2.jpg "Photo 2")



